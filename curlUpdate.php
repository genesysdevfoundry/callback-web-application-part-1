<?php
// Get the data we passed through the URL (the new time and callback ID) and append it to the URL
$sid = $_GET['sid'];
$url = 'http://demosrv.genesyslab.com:8010/genesys/1/service/callback/samples/';
$url .= $sid;
 
$fields = array(
 '_new_desired_time' => $_GET['updatedTime'] // API parameter name for new desired time
 );
 
$field_string = http_build_query($fields);
 
 
// Configure cURL
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url); 
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); // Note that we have replaced this option with PUT
curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string);
 
 
// Get Response 
$response = curl_exec($ch);
 
if (!$response) {
 die("Connection Failure");
}
// Close connection
curl_close($ch);
 
$result = "Your request was received.";
 
echo $result; // there is no server response so I gave it a custom message - NO error handling here right now