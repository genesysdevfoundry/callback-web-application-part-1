<?php
 
/* Below you will see the URL we're posting to for the initial callback. This is how you should format the URL:
* http://host:port/{base-web-application}/service/callback/{callback-execution-name}
*/
 
$url = 'http://demosrv.genesyslab.com:8010/genesys/1/service/callback/samples'; // MAKE SURE to edit this
 
/* This is the array of data we're posting to the server. Note that we are using the API parameter names with the 
* exception of 'callback_reason'. The parameter 'callback_reason' is custom attached data that can be retrieved via
* the Storage API or by customizing Workspace. I will be discussing how to utilize it in a follow-up tutorial. 
*/
 
$fields = array(
 '_customer_number' => $_POST['customer_number'],
 '_desired_time' => $_POST['desired_time'],
 '_usr_customer_name' => $_POST['customer_name']
 );
 
 
// URL-encode the query string
$field_string = http_build_query($fields);
 
// Configure cURL 
$ch = curl_init(); // creates a cURL resource
 
// Set your options
curl_setopt($ch, CURLOPT_URL, $url); // sets the cURL URL
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // set to true so we can collect the callback ID from the server
curl_setopt($ch, CURLOPT_POST, 1); // set POST to true
curl_setopt($ch, CURLOPT_POSTFIELDS, $field_string); // sends the query
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Set this to true if you've got SSL. Be sure to handle errors.
 
// Get response 
$response = curl_exec($ch);
 
// If there's no response, we'll exit the script and print a message indicating failure
if (!$response) {
 die("Connection Failure");
}
 
// Close the connection
curl_close($ch);
 
echo $response; // Send the response back to AJAX function
 
/* Sidenote: I don't recommend using closing PHP tags in pure PHP documents. 
 * If you're interested in why, read: http://php.net/manual/en/language.basic-syntax.phptags.php
*/