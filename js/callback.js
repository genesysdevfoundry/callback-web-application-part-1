$(document).ready(function () {

//STEP 1. Build the AJAX request function

    function callBack(type, url, data, dtype, result) {

        /* We're passing 5 arguments to the AJAX function .
         
         * TYPE: We need to use different methods (POST, PUT, and DELETE) depending on which callback function we want.
		 
		 * URL: The URL changes based on which method we need - POST, PUT, or DELETE.   
         
         * DATA: We need to post different data to the different pages.
         
         * DTYPE: To send the initial request, the data needs to be sent in JSON format. We don't want JSON for updating or deleting the callback.
         
         * RESULT: This is for you to post custom success/failure messages. I'm just doing something basic now, without error handling. */

        $.ajax({
            type: type,
            url: url,
            data: data,
            dataType: dtype,
            success: function (msg) {

                if (msg['_id']) {

                    var callId = msg['_id']; // on the first callback, we will get the id and save it in the ID input

                }

                else {

                    var callId = $('#id').val(); // this keeps the ID value in the input

                }
                ;

                $('#id').val(callId); // This collects the ID sent from the server and puts it into the corresponding input.

                var myMsg = JSON.stringify(msg); // This formats the server response so it's a bit more readable.

                $('#responseData').html("Your callback was " + result + "." + "<br /><br />" + "Additional Details::: " + myMsg);

            },
            error: function (msg) {

                var myMsg = JSON.stringify(msg);

                $('#responseData').html("ERROR::: " + myMsg);

            }

        });
    }

//STEP 2. Submits the initial callback request when Submit button is clicked.

        $('#submit').on("click", function (e) {

            e.preventDefault(); // add this to prevent the form's default action from being triggered
            
            var phoneInput = $('input[name="customer_number"]').val();

            var errormsg = "Please enter a valid 10 digit phone number";

            /* Below is the basic client-side validation I made to check if the phone number is 10 digits.
             
             * If it's 10 digits, it will submit the request. Otherwise you will get an error message.
             
             * You can add whatever validation later on.
             
             */

            if (phoneInput.length == 10) {

                var data = $('#callbackForm').serialize(); // Serializes all the form data into the data variable

                /* The callBack function below is invoked with 4 arguments.
                 
                 * To create a callback, the 'POST' method is used.
                 
                 * 'curl.php' is how we will POST the data to the server
                 
                 * 'json' is the datatype being sent
                 
                 * 'received' is the response we will get if the server receives the callback (remember, minimal error handling).
                 
                 */

                callBack('POST', 'curl.php', data, 'json', 'received');

            }

            else {

                alert(errormsg); // If the phone number doesn't pass validation, we will get this error.

            }

        });


//STEP 3. Updates callback when the Update button is clicked.

        $('#update').on("click", function (e) {

            e.preventDefault(); // add this to prevent the form's default action from being triggered

            var data = $('#modifyRequest').serialize(); // Serializes all the form data into the data variable

            /* In addition to setting the data variable, we're going to do something a little different with the
             
             * posting page. In order to update the time, we need to use the PUT method, but in doing so, we
             
             * won't be able to POST the necessary data to the page. To work around this, we will pass the data
             
             * via GET in the URL. Hence, as you can see below, the 'update' and 'data' variables are concatenated.
             
             */

            var update = 'curlUpdate.php?' + data; // We are passing the data via GET.

            /* The callBack function below is invoked with 4 arguments.
             
             * To update a callback, the 'PUT' method is used.
             
             * 'update' is the variable we set above that calls curlUpdate.php plus the data we’re passing with GET
             
             * We're omitting the datatype with ''.
             
             * 'rescheduled' is the message we're setting if the server receives the update (remember, minimal error handling).
             
             */

            callBack('PUT', update, data, '', 'rescheduled');

        });

//STEP 4. Cancels the callback when the Cancel button is clicked.

        $('#delete').on("click", function (e) {

            e.preventDefault(); // add this to prevent the form's default action from being triggered

            var data = $('#modifyRequest').serialize();

// We are going to do the same thing here as we did in Step 3 - use GET to pass the data to curlCancel.php.

            var deleteURL = 'curlCancel.php?' + data;

            /* The callBack function below is invoked with 4 arguments.
             
             * To cancel/delete a callback, the 'DELETE' method is used.
             
             * 'deleteURL' is the variable we set above that calls curlCancel.php plus the data we’re passing with GET
             
             * We're omitting the datatype with ''.
             
             * 'deleted' is the message we're setting if the server receives the deletion (minimal error handling).
             
             */

            callBack('DELETE', deleteURL, data, '', 'deleted');

        });


}); //close document ready