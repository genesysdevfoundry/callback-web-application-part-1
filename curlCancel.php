<?php
 
// This time we're just passing the callback ID and appending it to the URL
 
$sid = $_GET['sid'];
$url = 'http://demosrv.genesyslab.com:8010/genesys/1/service/callback/samples/';
$url .= $sid;
 
//Note that we aren't passing any data to the database so I removed the query string.
 
// Configure cURL
 
$ch = curl_init();
 
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE"); // Make sure to use the DELETE method
 
// Get Response
 
$response = curl_exec($ch);
 
if (!$response) {
 
die("Connection Failure");
 
}
 
// Close connection
 
curl_close($ch);
 
echo $response;