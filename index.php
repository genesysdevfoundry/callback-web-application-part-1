<!DOCTYPE html>
<html>
    <head>
        <title>Callback Demo</title>
               <meta charset="windows-1252">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/styles.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/callback.js"></script>
    </head>
    <body>
        <div class="container">

            <div>

                <h2>Request a callback</h2>

                <form id="callbackForm" method="POST">

                    <!-- The form action is omitted because we will be using AJAX to push the request to the CURL pages. -->

                    <input type="text" name="customer_name" value="" placeholder="Name">

                    <!-- 'customer_name' isn't required by the API, but you will probably want to add it. -->

                    <input id="callbackPhone" type="tel" name="customer_number" placeholder="Enter your callback phone number" value="">

                    <!-- This is the only REQUIRED parameter you need to generate a callback request. -->

                    <input id="desiredTime" type="text" name="desired_time" placeholder="Desired callback time start" value="2015-11-23T18:00:00.000Z">

                    <!--Even though desired time is not required, you should still add it in order to see the update functionality in this tutorial. The server requires the format ISO 8601 "yyyy-MM-ddTHH:mm:ss.SSSZ". In this sample, I have defaulted the input value to this format so we don't have to type it in. You can check out http://momentjs.com for a nice way of displaying the time format. Make sure this value is later than the current date. -->

                    <input id="submit" type="submit" value="Submit" >

                    <!-- By clicking this submit, the JS file we’re going to create will do a very basic validation and then POST via AJAX to our curl.php page. Remember, 'customer_number' is the only required parameter to successfully obtain an ID. -->

                </form>

            </div>

            <div>

                <h2>Update your callback request</h2>

                <form id="modifyRequest" method="POST">

                    <div>

                        <div>This is your callback ID number</div>

                        <input id="id" type="text" name="sid" placeholder="ID" value="">

                        <!-- The input above will store the callback ID. You will need this number to make changes to the callback. -->

                    </div>

                    <br><br>

                    <div>

                        <div>To reschedule your call back, enter the new time here and click 'Update'.</div>

                        <input id="updatedTime" type="text" name="updatedTime" placeholder="New callback time " value="2015-11-24T19:00:00.000Z">

                        <!-- This is where you can modify the time of the callback. It's the same format as the first input. Again, make sure the date is later than the current date. -->

                        <input id="update" type="submit" value="Update" >

                        <!-- By clicking this, the JS file will use AJAX to send the data to curlUpdate.php, which uses PUT to update the callback time. -->

                    </div>

                    <br><br>

                    <div>

                        <div>To Cancel your callback, click 'Cancel Callback'</div>

                        <input id="delete" type="submit" value="Cancel Callback" >

                        <!-- By clicking this, the JS file will use AJAX to send the callback ID to curlCancel.php, which uses the ID and the DELETE method to cancel the callback. -->

                    </div>
                    
                </form>
                
                <br><br>

                <div id="responseData">
                    <!-- The server response will be printed out in the div below. -->
                </div>

            </div>
    </body>
</html>
